<?php

/*
 * Presmerovani po registraci FOSUserBundle
 */

namespace dlouhy\UserBundle\EventListener;

use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RegistrationConfirmListener implements EventSubscriberInterface
{

	private $router;

	public function __construct(Router $router)
	{
		$this->router = $router;
	}


	/**
	 * {@inheritDoc}
	 */
	public static function getSubscribedEvents()
	{
		return array(
			FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess'
		);
	}


	public function onRegistrationSuccess(FormEvent $event)
	{
		$url = $this->router->generate('admin_users_list');

		$event->setResponse(new RedirectResponse($url));
	}
	

}
