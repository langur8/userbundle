<?php

namespace dlouhy\UserBundle\Controller;

use dlouhy\SimpleCRUDBundle\Controller\BaseCRUDController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use dlouhy\UserBundle\Entity\User;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\GetResponseUserEvent;

class UserController extends BaseCRUDController
{
	
	/**
     * @Route("/admins", name="admin_user_list")
     */
    public function listAction(Request $request)
    {		
		return parent::listAction($request);
    }
	
	
	/**
	 * @Route("/users/switch_active/{id}", name="admin_user_switch_active") 
	 */
	public function switchActiveAction(Request $request, $id)
	{
		return parent::switchBoolAction($request, $id, 'enabled');
	}

	
	/**
	 * @Route("/users/delete/{id}", name="admin_user_delete") 
	 */
	public function deleteAction(Request $request, $id)
	{
		if (!$id) {
			throw $this->createNotFoundException('Nepredano ID');
		}
				
		$userManager = $this->get('fos_user.user_manager');
		$user = $userManager->findUserBy(array('id' => $id));	
		
		if (!$user instanceof User) {
			throw $this->createNotFoundException('Uzivatel neexistuje: '.$id);
		}
		
		$userManager->deleteUser($user);
						
		return new Response(json_encode(array('id' => $id)));
	}
	
	
	/**
     * @Route("/administrator/add", name="admin_user_add")
     */
    public function addAction(Request $request)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);
		
		return $this->render('dlouhyUserBundle:User:add.html.twig', array('form' => $form->createView()));
    }		
	
	/**
     * @Route("/user/save", name="admin_user_save", defaults={"id" = null}, requirements={"id": "\d+"})
     */	
	public function saveAction(Request $request, $id)
	{
       /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

		$message = '';		
		$redirect = '';
		$returnCode = 400;
        if ($form->isValid()) {
			
            //$event = new FormEvent($form, $request);
            //$dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

			$user->addRole('ROLE_ADMIN');
            $userManager->updateUser($user);

			$redirect = $this->generateUrl('admin_user_list');			
			$returnCode = 200;
        }	
		
		return new JsonResponse(array(
			'message' => $message,			
			'replace' => true,
			'redirect' => $redirect,
			'html' => $this->renderView('dlouhyUserBundle:User:form.html.twig', array(
				'form' => $form->createView(),
			))), $returnCode);		
		
	}
	
	
	protected function findAll()
	{
		return $this->getDoctrine()->getRepository('dlouhy\UserBundle\Entity\User')->findByRole('ROLE_ADMIN');        
	}
}
