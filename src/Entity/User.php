<?php

namespace dlouhy\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="dlouhy\UserBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(type="string", length=100, nullable=true)
	 * 
	 * @Assert\NotBlank(message="Please enter your name.", groups={"Registration", "Profile"})
	 * @Assert\Length(
	 *     min=3,
	 *     max="255",
	 *     minMessage="The name is too short.",
	 *     maxMessage="The name is too long.",
	 *     groups={"Registration", "Profile"})
	 */
	protected $name;

	/**
	 * @ORM\Column(type="boolean", name="visual_editor")
	 */
	protected $visualEditor = true;

	public function __construct()
	{
		parent::__construct();
		// your own logic
	}
	
	public function __toString()
	{
		return $this->name;
	}

	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return User
	 */
	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string 
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Set visualEditor
	 *
	 * @param boolean $visualEditor
	 * @return User
	 */
	public function setVisualEditor($visualEditor)
	{
		$this->visualEditor = $visualEditor;

		return $this;
	}

	/**
	 * Get visualEditor
	 *
	 * @return boolean 
	 */
	public function getVisualEditor()
	{
		return $this->visualEditor;
	}

}
