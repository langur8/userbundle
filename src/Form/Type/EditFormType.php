<?php

/**
 * Rozsireni FOSUserBundle editace
 */

namespace dlouhy\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditFormType extends AbstractType
{

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('name', null, array('label' => 'form.name', 'translation_domain' => 'FOSUserBundle'))
				->add('username', null, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle'))
				->add('email', 'email', array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))		
				->add('visual_editor', 'checkbox', array('label' => 'form.visual_editor', 'translation_domain' => 'FOSUserBundle', 'required' => false))
				->add('save', 'submit', array('label' => 'OK'));
	}


	public function getName()
	{
		return 'dlouhy_user_edit';
	}


	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'validation_groups' => array('Profile', 'Default'),
		));
	}


}
