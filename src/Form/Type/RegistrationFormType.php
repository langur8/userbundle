<?php

/**
 * Rozsireni FOSUserBundle registrace
 */

namespace dlouhy\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
			->add('name', null, array(
				'label' => 'form.name',
				'translation_domain' => 'FOSUserBundle'))
			->add('visualEditor', 'hidden')
			->add('save', 'submit', array('label' => 'OK'));
    }

    public function getParent()
    {
        return 'fos_user_registration';
    }

    public function getName()
    {
        return 'dlouhy_user_register';
    }
}