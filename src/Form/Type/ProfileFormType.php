<?php

/**
 * Rozsireni FOSUserBundle registrace
 */

namespace dlouhy\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder->add('name', null, array(
			'label' => 'form.name',
            'translation_domain' => 'FOSUserBundle'));
		
//        $builder->add('visual_editor', 'checkbox', array(
//			'label' => 'form.visual_editor',
//            'translation_domain' => 'FOSUserBundle',
//			'required'    => false));
    }

    public function getParent()
    {
        return 'fos_user_profile';
    }

    public function getName()
    {
        return 'dlouhy_user_profile';
    }
}